.PHONY: recreate-cluster setup-ingress-controller first-demo apply-certs setup-gateway-controller get-gateway-service first-gateway-demo tunnel-gateway

recreate-cluster:
	kind delete cluster -n demo-cluster
	kind create cluster --config kind-cluster-config.yml -n demo-cluster

setup-ingress-controller:
	# setup contour
	kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
	# patch daemonset
	kubectl patch daemonsets -n projectcontour envoy -p '{"spec":{"template":{"spec":{"nodeSelector":{"ingress-ready":"true"},"tolerations":[{"key":"node-role.kubernetes.io/control-plane","operator":"Equal","effect":"NoSchedule"},{"key":"node-role.kubernetes.io/master","operator":"Equal","effect":"NoSchedule"}]}}}}'

first-demo: setup-ingress-controller apply-certs
	kubectl apply -f 01-simple-ingress/app.yml
	kubectl apply -f 01-simple-ingress/ingress.yml

certs/%.key certs/%.crt:
	openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout certs/$*.key -out certs/$*.crt -subj "/CN=localhost" -days 365

create-ssl-certs: certs/tls.key certs/tls.crt

apply-certs:
	kubectl apply -f 01-simple-ingress/namespace.yml
	kubectl create secret tls ingress-local-tls --cert=certs/tls.crt --key=certs/tls.key --namespace 01-simple-ingress

setup-gateway-controller:
	kubectl apply -f https://github.com/envoyproxy/gateway/releases/download/v0.2.0/install.yaml

first-gateway-demo: 
	kubectl apply -f 02-simple-gateway/namespace.yml
	kubectl apply -f 02-simple-gateway/gateway.yml
	kubectl apply -f 02-simple-gateway/app.yml

tunnel-gateway:
	NAMESPACE ?= "02-simple-gateway"
	$(eval GATEWAY_SERVICE=$(shell kubectl get svc --all-namespaces=true --selector=gateway.envoyproxy.io/owning-gateway-namespace=$(NAMESPACE),gateway.envoyproxy.io/owning-gateway-name=simple-gateway -o jsonpath='{.items[0].metadata.name}'))
	@echo gatway name is $(GATEWAY_SERVICE);
	@kubectl -n envoy-gateway-system port-forward service/$(GATEWAY_SERVICE) 8080:8080;

tls-tunnel-gateway:
	NAMESPACE ?= "07-tls"
	$(eval GATEWAY_SERVICE=$(shell kubectl get svc --all-namespaces=true --selector=gateway.envoyproxy.io/owning-gateway-namespace=$(NAMESPACE),gateway.envoyproxy.io/owning-gateway-name=tls-gateway -o jsonpath='{.items[0].metadata.name}'))
	@echo gatway name is $(GATEWAY_SERVICE);
	@kubectl -n envoy-gateway-system port-forward service/$(GATEWAY_SERVICE) 8003:8003;

test-tls:
	curl -v -HHost:www.example.com --resolve "www.example.com:8003:127.0.0.1" --cacert certs/example.com.crt https://www.example.com:8003/

gateway-secret:
	NAMESPACE ?= "06-tls"
	kubectl create secret tls example-cert --key=certs/www.example.com.key --cert=certs/www.example.com.crt -n $(NAMESPACE)